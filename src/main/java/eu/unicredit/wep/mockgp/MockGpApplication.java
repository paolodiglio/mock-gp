package eu.unicredit.wep.mockgp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockGpApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockGpApplication.class, args);
	}

}
