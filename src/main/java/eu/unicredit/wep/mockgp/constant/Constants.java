package eu.unicredit.wep.mockgp.constant;

public class Constants {
  public static final String GLOBAL_ID = "globalID";
  public static final String INVALID_REQUEST = "invalid_request";
  public static final String BASIC_AUTH_ENDPOINT = "/basic_auth";
  public static final String CHANGE_PIN_ENDPOINT = "/change_pin";
  public static final String RECOVERY_PIN_ENDPOINT = "/recovery_pin";
}
