package eu.unicredit.wep.mockgp.utility;

import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.PasswordLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.stream.Collectors;

public class KeyStoreManager {
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private JWKSet jwkSet;

    private RSAPublicKey encryptionPublicKey;
    private RSAPrivateKey encryptionPrivateKey;
    private JWSVerifier publicKey;

    private JWSSigner privateKey;

    public KeyStoreManager(String keystoreLocation, String keystorePassword, String tokenAlias, String tokenPassword) {

        try  {
            FileInputStream is = new FileInputStream(keystoreLocation);
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(is, keystorePassword.toCharArray());

            LOG.debug("Reading token keys");

            // Loading token signing/verifying keys
            Key key = keystore.getKey(tokenAlias, tokenPassword.toCharArray());
            // Get certificate of public key
            Certificate cert = keystore.getCertificate(tokenAlias);

            // Get public key
            PublicKey tokenPublicKey = cert.getPublicKey();



            // Retrieving JWKSet -> it will retrieve only the keys with password = keystoreTokenKeysPw
            PasswordLookup pwLookup = name -> tokenPassword.toCharArray();

            JWKSet jwkSetFull = JWKSet.load(keystore, pwLookup);

            // Removing all other keys from JWKSet, using only the one configured
            this.jwkSet =
                    new JWKSet(
                            jwkSetFull.getKeys().stream()
                                    .filter(jwk -> jwk.getKeyID().equals(tokenAlias))
                                    .collect(Collectors.toList()));

            // Return a key pair + certificate
            this.privateKey = new RSASSASigner((PrivateKey) key);
            this.publicKey = new RSASSAVerifier((RSAPublicKey) tokenPublicKey);
            this.encryptionPublicKey = (RSAPublicKey) tokenPublicKey;

            PrivateKey tokenPrivateKey = (PrivateKey) key;
            this.encryptionPrivateKey = (RSAPrivateKey) tokenPrivateKey;


        } catch (IOException e) {
            LOG.debug(e.getMessage());
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException | KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }


    public JWKSet getJwkSet() {
        return jwkSet;
    }

    public JWSVerifier getPublicKey() {
        return publicKey;
    }

    public JWSSigner getPrivateKey() {
        return privateKey;
    }

    public RSAPublicKey getEncryptionPublicKey() {
        return encryptionPublicKey;
    }

    public RSAPrivateKey getEncryptionPrivateKey() {
        return encryptionPrivateKey;
    }


}
