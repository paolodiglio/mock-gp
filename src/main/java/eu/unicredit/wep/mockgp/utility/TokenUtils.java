package eu.unicredit.wep.mockgp.utility;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import static eu.unicredit.wep.mockgp.constant.Constants.GLOBAL_ID;

@Component
public class TokenUtils {

    @Value("${keystore.token.keys.alias}")
    private String tokenAlias;

    @Autowired
    private KeyStoreManager keyStoreManager;

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    public String getAccessToken(UUID uuid)
            throws Exception {

        Instant now = Instant.now();

        Date expirationTime =
                Date.from(
                        now.plus(
                                Long.parseLong("480"),
                                ChronoUnit.MINUTES));
        System.out.println(expirationTime.toInstant().toString());

        JWTClaimsSet jwtClaims =
                retrieveClaims(uuid, expirationTime, now);

        JWEHeader header = new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128GCM)
                .customParam("iss","gp")
                .build();
        EncryptedJWT jwt = new EncryptedJWT(header, jwtClaims);
        RSAEncrypter encrypter = new RSAEncrypter(keyStoreManager.getEncryptionPublicKey());
        jwt.encrypt(encrypter);




        return jwt.serialize();

    }

    public EncryptedJWT encrypt(String accessToken) throws Exception {

        SignedJWT signedAccessToken = SignedJWT.parse(accessToken);
        JWTClaimsSet claims = signedAccessToken.getJWTClaimsSet();
        JWEHeader header = new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128GCM)
                .customParam("iss","iam")
                .build();
        EncryptedJWT jwt = new EncryptedJWT(header, claims);
        RSAEncrypter encrypter = new RSAEncrypter(keyStoreManager.getEncryptionPublicKey());
        jwt.encrypt(encrypter);
        return jwt;
    }


    public JWTClaimsSet retrieveClaims(
            UUID globalId,
            Date expirationTime,
            Instant now) {

        JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();


        builder
                .claim(GLOBAL_ID, globalId.toString())
                .expirationTime(expirationTime)
                .notBeforeTime(Date.from(now))
                .issueTime(Date.from(now))
                .jwtID(UUID.randomUUID().toString());
        return builder.build();
    }
}
