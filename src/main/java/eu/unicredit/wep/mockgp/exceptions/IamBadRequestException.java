package eu.unicredit.wep.mockgp.exceptions;

public class IamBadRequestException extends RuntimeException {

    private String errorDescription;

    public IamBadRequestException(String errorMessage, String errorDescription) {
        super(errorMessage);
        this.errorDescription = errorDescription;
    }

    public IamBadRequestException(String errorMessage) {
        super(errorMessage);
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
