package eu.unicredit.wep.mockgp.controllerimpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jwt.EncryptedJWT;
import eu.unicredit.wep.mockgp.controller.GPController;
import eu.unicredit.wep.mockgp.exceptions.IamBadRequestException;
import eu.unicredit.wep.mockgp.infoToSend.*;
import eu.unicredit.wep.mockgp.utility.KeyStoreManager;
import eu.unicredit.wep.mockgp.utility.TokenUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Optional;
import java.util.UUID;

import static eu.unicredit.wep.mockgp.constant.Constants.*;

@Data
@RestController
public class GPControllerImpl implements GPController {

  @Autowired private TokenUtils tokenUtils;

  @Autowired private KeyStoreManager keyStoreManager;

  private static final Logger logger = LoggerFactory.getLogger(GPControllerImpl.class);

  @Value("${IAM.host}")
  private String iamHost;

  private final RestTemplate restTemplate;

  @Autowired
  public GPControllerImpl(RestTemplateBuilder builder) {
    this.restTemplate = builder.build();
  }

  @Override
  public ResponseEntity<String> sendTokenToIAM(String token) throws Exception {

    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

    EncryptedJWT signedAccessToken = EncryptedJWT.parse(token);

    if (!signedAccessToken.getHeader().getCustomParam("iss").equals("gp")) {

      EncryptedJWT jwt = EncryptedJWT.parse(token);
      RSADecrypter decrypter = new RSADecrypter(keyStoreManager.getEncryptionPrivateKey());
      jwt.decrypt(decrypter);

      headers.add("Authorization", token);

    } else {

      HttpEntity<String> httpEntity = new HttpEntity<>(token);
      RestTemplate restTemplate = new RestTemplate();
      ResponseEntity<String> responseEntity =
          restTemplate.exchange(iamHost, HttpMethod.POST, httpEntity, String.class);
      Token tokenReceived = new ObjectMapper().readValue(responseEntity.getBody(), Token.class);
      headers.add("Authorization", tokenUtils.encrypt(tokenReceived.getAccess_token()).serialize());
    }
    return new ResponseEntity<>("ciao utente", headers, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<String> getGpToken() throws Exception {
    UUID globalId = UUID.randomUUID();
    String token = tokenUtils.getAccessToken(globalId);
    return new ResponseEntity<>(token, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<PortalUserResponse> getUserInfo(String globalId) {

    AdditionalProperties additionalProperties = new AdditionalProperties();
    additionalProperties.setPhone("1112223334");
    additionalProperties.setMobilePhone("4445556667");

    PortalUserResponse portalUserResponse = new PortalUserResponse();
    portalUserResponse.setAdditionalProperties(additionalProperties);
    portalUserResponse.setDateOfBirth("01/01/1970");
    portalUserResponse.setFirstName("prova");
    portalUserResponse.setLastName("test");
    portalUserResponse.setLegalSourceName("testdiprova");

    return new ResponseEntity<>(portalUserResponse, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<String> getKey(String key, CredentialsBasicAuth body) throws Exception {

    byte[] encoded = Base64.decodeBase64(key.replace(" ", "+"));

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    PublicKey pubKey = keyFactory.generatePublic(keySpec);

    // Creating a Cipher object
    Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
    // Initializing a Cipher object
    cipher.init(Cipher.ENCRYPT_MODE, pubKey);

    try {
      checkBodyBasicAuth(body);
    } catch (Exception e) {
      return new ResponseEntity<>("Error on body", HttpStatus.BAD_REQUEST);
    }

    // Add data to the cipher
    byte[] input = body.getUsername().getBytes();
    /*byte[] input = "F0047330".getBytes();*/
    cipher.update(input);

    // encrypting the data
    byte[] cipherText = cipher.doFinal();

    CredentialsBasicAuth credentialsBasicAuth = new CredentialsBasicAuth();
    byte[] input2 = body.getPin().getBytes();
    // byte[] input2 = "Aa000000".getBytes();
    cipher.update(input2);
    byte[] cipherText2 = cipher.doFinal();

    String usernameString = Base64.encodeBase64String(cipherText);
    String passwordString = Base64.encodeBase64String(cipherText2);

    logger.info(usernameString);
    logger.info(String.valueOf(cipherText2.length));
    credentialsBasicAuth.setUsername(usernameString);
    credentialsBasicAuth.setPin(passwordString);
    credentialsBasicAuth.setClientId(body.getClientId());
    credentialsBasicAuth.setNonce(body.getNonce());
    credentialsBasicAuth.setIdp(body.getIdp());
    credentialsBasicAuth.setIpAddress(body.getIpAddress());

    // Set the headers you need send
    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-Forwarded-For", "10.112.160.170");

    HttpEntity httpEntity = new HttpEntity(credentialsBasicAuth, headers);

    return restTemplate.postForEntity(iamHost + BASIC_AUTH_ENDPOINT, httpEntity, String.class);
  }

  @Override
  public ResponseEntity<String> changePin(String key, CredentialsChangePin body)
      throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
          BadPaddingException, InvalidKeyException, InvalidKeySpecException {

    byte[] encoded = Base64.decodeBase64(key.replace(" ", "+"));

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    PublicKey pubKey = keyFactory.generatePublic(keySpec);
    // Creating a Cipher object
    Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
    // Initializing a Cipher object
    cipher.init(Cipher.ENCRYPT_MODE, pubKey); // Add data to the cipher
    try {
      checkBodyChangePin(body);
    } catch (Exception e) {
      return new ResponseEntity<>("Error on body", HttpStatus.BAD_REQUEST);
    }

    byte[] input2 = body.getOldPin().getBytes(); // OLD PIN
    cipher.update(input2);
    byte[] cipherText2 = cipher.doFinal();

    byte[] input3 = body.getNewPin().getBytes(); // NEW PIN
    cipher.update(input3);
    byte[] cipherText3 = cipher.doFinal();

    String oldPin = Base64.encodeBase64String(cipherText2);
    String newPin = Base64.encodeBase64String(cipherText3);

    CredentialsChangePin changePin = new CredentialsChangePin();
    changePin.setOldPin(oldPin);
    changePin.setNewPin(newPin);
    changePin.setNonce(body.getNonce());
    changePin.setClientId(body.getClientId());
    changePin.setOtp(body.getOtp());
    changePin.setIpAddress(body.getIpAddress());

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-Forwarded-For", "10.112.160.170");

    HttpEntity httpEntity = new HttpEntity(changePin, headers);
    return restTemplate.postForEntity(iamHost + CHANGE_PIN_ENDPOINT, httpEntity, String.class);
  }

  @Override
  public ResponseEntity<String> sendRecoveryUser(String key, CredentialsRecoveryUser body)
      throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
          BadPaddingException, InvalidKeyException, InvalidKeySpecException {

    byte[] encoded = Base64.decodeBase64(key.replace(" ", "+"));

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    PublicKey pubKey = keyFactory.generatePublic(keySpec);
    // Creating a Cipher object
    Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
    // Initializing a Cipher object
    cipher.init(Cipher.ENCRYPT_MODE, pubKey); // Add data to the cipher

    byte[] input = body.getUserEmail().getBytes();
    cipher.update(input);
    // encrypting the data
    byte[] cipherText = cipher.doFinal();

    byte[] input2 = body.getPhoneNumber().getBytes(); // OLD PIN
    cipher.update(input2);
    byte[] cipherText2 = cipher.doFinal();

    String email = Base64.encodeBase64String(cipherText);
    String phoneNumber = Base64.encodeBase64String(cipherText2);

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-Forwarded-For", "10.112.160.170");

    CredentialsRecoveryUser credentialsRecoveryUser = new CredentialsRecoveryUser();
    credentialsRecoveryUser.setUserEmail(email);
    credentialsRecoveryUser.setPhoneNumber(phoneNumber);
    credentialsRecoveryUser.setClientId(body.getClientId());
    credentialsRecoveryUser.setNonce(body.getNonce());
    credentialsRecoveryUser.setIpAddress(body.getIpAddress());

    HttpEntity httpEntity = new HttpEntity(credentialsRecoveryUser, headers);
    return restTemplate.postForEntity(iamHost + "/recovery_user", httpEntity, String.class);
  }

  @Override
  public ResponseEntity<String> recovery2FA(String key, Credentials2FARecovery body)
      throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
          InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    byte[] encoded = Base64.decodeBase64(key.replace(" ", "+"));

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    PublicKey pubKey = keyFactory.generatePublic(keySpec);
    // Creating a Cipher object
    Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
    // Initializing a Cipher object
    cipher.init(Cipher.ENCRYPT_MODE, pubKey); // Add data to the cipher

    byte[] input = body.getUserEmail().getBytes();
    cipher.update(input);
    // encrypting the data
    byte[] cipherText = cipher.doFinal();

    byte[] input2 = body.getOtp().getBytes(); // OLD PIN
    cipher.update(input2);
    byte[] cipherText2 = cipher.doFinal();

    String email = Base64.encodeBase64String(cipherText);
    String otp = Base64.encodeBase64String(cipherText2);

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-Forwarded-For", "10.112.160.170");

    Credentials2FARecovery credentials2FARecovery = new Credentials2FARecovery();
    credentials2FARecovery.setUserEmail(email);
    credentials2FARecovery.setOtp(otp);
    credentials2FARecovery.setRecoveryType(body.getRecoveryType());
    credentials2FARecovery.setClientId(body.getClientId());
    credentials2FARecovery.setNonce(body.getNonce());
    credentials2FARecovery.setIpAddress(body.getIpAddress());

    HttpEntity httpEntity = new HttpEntity(credentials2FARecovery, headers);
    return restTemplate.postForEntity(iamHost + "/2factor_auth_recovery", httpEntity, String.class);
  }

  @Override
  public ResponseEntity<String> sendRecoveryPin(String key, CredentialsRecoveryPin body)
      throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
          InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

    byte[] encoded = Base64.decodeBase64(key.replace(" ", "+"));

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    PublicKey pubKey = keyFactory.generatePublic(keySpec);
    // Creating a Cipher object
    Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");
    // Initializing a Cipher object
    cipher.init(Cipher.ENCRYPT_MODE, pubKey); // Add data to the cipher

    byte[] input = body.getUserEmail().getBytes();
    cipher.update(input);
    // encrypting the data
    byte[] cipherText = cipher.doFinal();

    byte[] input2 = body.getNickName().getBytes();
    cipher.update(input2);
    byte[] cipherText2 = cipher.doFinal();

    byte[] input3 = body.getPhoneNumber().getBytes();
    cipher.update(input3);
    // encrypting the data
    byte[] cipherText3 = cipher.doFinal();

    String email = Base64.encodeBase64String(cipherText);
    String nickName = Base64.encodeBase64String(cipherText2);
    String phoneNumber = Base64.encodeBase64String(cipherText3);

    CredentialsRecoveryPin credentialsRecoveryPin = new CredentialsRecoveryPin();
    credentialsRecoveryPin.setUserEmail(email);
    credentialsRecoveryPin.setNickName(nickName);
    credentialsRecoveryPin.setPhoneNumber(phoneNumber);
    credentialsRecoveryPin.setNonce(body.getNonce());
    credentialsRecoveryPin.setIpAddress(body.getIpAddress());
    credentialsRecoveryPin.setClientId(body.getClientId());

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-Forwarded-For", "10.112.160.170");
    HttpEntity httpEntity = new HttpEntity(credentialsRecoveryPin, headers);

    return restTemplate.postForEntity(iamHost + "/recovery_pin", httpEntity, String.class);
  }

  void checkBodyBasicAuth(CredentialsBasicAuth body) throws IamBadRequestException {
    body =
        Optional.ofNullable(body)
            .orElseThrow(
                () -> new IamBadRequestException(INVALID_REQUEST, "Body must not be null"));

    Optional.ofNullable(body.getUsername())
        .orElseThrow(() -> new IamBadRequestException("Param username must not be null"));

    Optional.ofNullable(body.getPin())
        .orElseThrow(() -> new IamBadRequestException("Param pin must not be null"));

    Optional.ofNullable(body.getIdp())
        .orElseThrow(() -> new IamBadRequestException("Param idp must not be null"));

    Optional.ofNullable(body.getIpAddress())
        .orElseThrow(() -> new IamBadRequestException("Param ip_address must not be null"));

    Optional.ofNullable(body.getClientId())
        .orElseThrow(() -> new IamBadRequestException("Param client_id must not be null"));

    Optional.ofNullable(body.getNonce())
        .orElseThrow(() -> new IamBadRequestException("Param nonce must not be null"));
  }

  void checkBodyChangePin(CredentialsChangePin body) throws IamBadRequestException {
    body =
        Optional.ofNullable(body)
            .orElseThrow(
                () -> new IamBadRequestException(INVALID_REQUEST, "Body must not be null"));

    Optional.ofNullable(body.getOldPin())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param old_pin must not be null"));

    Optional.ofNullable(body.getNewPin())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param new_pin must not be null"));

    Optional.ofNullable(body.getOtp())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param otp must not be null"));

    Optional.ofNullable(body.getIpAddress())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param ip_address must not be null"));

    Optional.ofNullable(body.getClientId())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param client_id must not be null"));

    Optional.ofNullable(body.getNonce())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param nonce must not be null"));
  }

  void checkBodyRecoveryPin(CredentialsRecoveryPin body) {
    body =
        Optional.ofNullable(body)
            .orElseThrow(
                () -> new IamBadRequestException(INVALID_REQUEST, "Body must not be null"));

    Optional.ofNullable(body.getNickName())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param nick_name must not be null"));

    Optional.ofNullable(body.getClientId())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param client_id must not be null"));

    Optional.ofNullable(body.getUserEmail())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param user_email must not be null"));

    Optional.ofNullable(body.getPhoneNumber())
        .orElseThrow(
            () ->
                new IamBadRequestException(INVALID_REQUEST, "Param phone_number must not be null"));

    Optional.ofNullable(body.getNonce())
        .orElseThrow(
            () -> new IamBadRequestException(INVALID_REQUEST, "Param nonce must not be null"));
  }
}
