package eu.unicredit.wep.mockgp;

import eu.unicredit.wep.mockgp.utility.KeyStoreManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${keystore.location}")
    private String keystoreLocation;

    @Value("${keystore.password}")
    private String keystorePassword;

    @Value("${keystore.token.keys.alias}")
    private String tokenAlias;

    @Value("${keystore.token.keys.password}")
    private String tokenPassword;

    @Bean
    public KeyStoreManager init() {

        return new KeyStoreManager(keystoreLocation,keystorePassword,tokenAlias,tokenPassword);
    }
}
