package eu.unicredit.wep.mockgp.controller;

import eu.unicredit.wep.mockgp.infoToSend.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;

@RequestMapping(produces = "application/json")
public interface GPController {

  @GetMapping("/getgpToken")
  default ResponseEntity<String> getGpToken() throws Exception {
    return null;
  }

  @PostMapping("/resource-request")
  default ResponseEntity<String> sendTokenToIAM(
      @RequestHeader(value = "Authorization") String token) throws Exception {
    return null;
  }

  @PostMapping("/sendBasicAuth")
  default ResponseEntity<String> getKey(
      @RequestParam(value = "key") String key, @RequestBody CredentialsBasicAuth body)
      throws Exception {
    return null;
  }

  @GetMapping("/userinfo")
  default ResponseEntity<PortalUserResponse> getUserInfo(
      @RequestParam(value = "globalID") String globalId) {
    return null;
  }

  @GetMapping("/sendChangePin")
  default ResponseEntity<String> changePin(
      @RequestParam(value = "key") String key, @RequestBody CredentialsChangePin body)
      throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException,
          IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
    return null;
  }

  @PostMapping("/sendRecoveryUser")
  default ResponseEntity<String> sendRecoveryUser(
      @RequestParam(value = "key") String key, @RequestBody CredentialsRecoveryUser body)
      throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
          BadPaddingException, InvalidKeyException, InvalidKeySpecException {
    return null;
  }

  @PostMapping("/send2FARecovery")
  default ResponseEntity<String> recovery2FA(
      @RequestParam(value = "key") String key, @RequestBody Credentials2FARecovery body)
      throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
          InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    return null;
  }

  @PostMapping("/sendRecoveryPin")
  default ResponseEntity<String> sendRecoveryPin(
      @RequestParam(value = "key") String key, @RequestBody CredentialsRecoveryPin body)
      throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
          InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    return null;
  }
}
