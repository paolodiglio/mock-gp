package eu.unicredit.wep.mockgp.infoToSend;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdditionalProperties implements Serializable {

    private static final long serialVersionUID = 2530690128384879724L;

    private String mobilePhone;
    private String phone;

}
