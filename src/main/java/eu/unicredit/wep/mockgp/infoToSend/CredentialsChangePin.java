package eu.unicredit.wep.mockgp.infoToSend;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CredentialsChangePin {

  @JsonProperty("old_pin")
  String oldPin;

  @JsonProperty("new_pin")
  String newPin;

  String otp;

  @JsonProperty("ip_address")
  String ipAddress;

  @JsonProperty("client_id")
  String clientId;

  String nonce;

  public String getOldPin() {
    return oldPin;
  }

  public String getNewPin() {
    return newPin;
  }

  public String getOtp() {
    return otp;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public String getClientId() {
    return clientId;
  }

  public String getNonce() {
    return nonce;
  }

  public void setOldPin(String oldPin) {
    this.oldPin = oldPin;
  }

  public void setNewPin(String newPin) {
    this.newPin = newPin;
  }

  public void setOtp(String otp) {
    this.otp = otp;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public void setNonce(String nonce) {
    this.nonce = nonce;
  }
}
