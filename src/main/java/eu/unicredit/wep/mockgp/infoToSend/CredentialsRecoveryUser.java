package eu.unicredit.wep.mockgp.infoToSend;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CredentialsRecoveryUser {

  @JsonProperty("phone_number")
  private String phoneNumber;

  @JsonProperty("user_email")
  private String userEmail;

  @JsonProperty("ip_address")
  private String ipAddress;

  @JsonProperty("client_id")
  private String clientId;

  private String nonce;

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getNonce() {
    return nonce;
  }

  public void setNonce(String nonce) {
    this.nonce = nonce;
  }
}
