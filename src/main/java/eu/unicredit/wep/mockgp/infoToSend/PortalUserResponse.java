package eu.unicredit.wep.mockgp.infoToSend;

import lombok.Data;

import java.io.Serializable;

@Data
public class PortalUserResponse implements Serializable {

    private static final long serialVersionUID = -8938739649335338013L;

    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String legalSourceName;
    private AdditionalProperties additionalProperties;

}
