package eu.unicredit.wep.mockgp.infoToSend;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Credentials2FARecovery {
    private String otp;

    @JsonProperty("user_email")
    private String userEmail;

    @JsonProperty("recovery_type")
    private String recoveryType;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("client_id")
    private String clientId;

    private String nonce;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getRecoveryType() {
        return recoveryType;
    }

    public void setRecoveryType(String recoveryType) {
        this.recoveryType = recoveryType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

}
