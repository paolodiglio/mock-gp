package eu.unicredit.wep.mockgp.infoToSend;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CredentialsBasicAuth {

  private String username;
  private String pin;
  private String idp;

  @JsonProperty("ip_address")
  private String ipAddress;

  @JsonProperty("client_id")
  private String clientId;

  private String nonce;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPin() {
    return pin;
  }

  public void setPin(String pin) {
    this.pin = pin;
  }

  public String getIdp() {
    return idp;
  }

  public void setIdp(String idp) {
    this.idp = idp;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getNonce() {
    return nonce;
  }

  public void setNonce(String nonce) {
    this.nonce = nonce;
  }
}
