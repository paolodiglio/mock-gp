package eu.unicredit.wep.mockgp.infoToSend;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CredentialsRecoveryPin {

  @JsonProperty("phone_number")
  private String phoneNumber;

  @JsonProperty("user_email")
  private String userEmail;

  @JsonProperty("ip_address")
  private String ipAddress;

  @JsonProperty("client_id")
  private String clientId;

  @JsonProperty("nick_name")
  private String nickName;

  private String nonce;
}
